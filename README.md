This is a repo containing code and data accompanying the paper "Susceptibility of Protein Methionine Oxidation in Response to Hydrogen Peroxide Treatment–Ex Vivo Versus In Vitro: A Computational Insight".
(Antioxidants 2020, 9(10), 987; https://doi.org/10.3390/antiox9100987).
